
import time
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import pathlib
import asyncio
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

class ParseBot:
    def __init__(self):
        self.driver = None

        self.status = "200"
        self.cookies = None
        self.options = webdriver.ChromeOptions()
        script_directory = pathlib.Path().absolute()
        self.options.add_argument("--window-size=2100,1080")
        self.options.add_argument("--start-maximized")
        self.options.add_argument("--lang=ru-ru")
        self.options.add_argument(f"user-data-dir={script_directory}\\userdata")
        #  Для хранения кук ( и другого ) в папке проекта
        self.options.add_argument("--allow-profiles-outside-user-dir")
        self.options.add_argument("--enable-profile-shortcut-manager")
        self.options.add_argument(f"user-data-dir={script_directory}\\userdata")
        self.options.add_argument("--profile-directory=Profile 1")

        self.Service = Service(executable_path=ChromeDriverManager().install())
        self.driver = webdriver.Chrome(
            service=self.Service, options=self.options
        )

    async def close(self):
        try:
            self.driver.close()
            self.driver.quit()
            self.driver = None
        except:
            pass

pb = ParseBot()
pb.driver.get('https://habr.com/ru/articles/')
time.sleep(3)
H2 = pb.driver.find_element(By.CSS_SELECTOR, 'h2')
print(H2.text)
pb.driver.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[1]/div/nav/a[3]").click()