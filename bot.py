import os

from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from dotenv import load_dotenv


load_dotenv()


class Config:
    BOT_TOKEN = os.getenv("BOT_TOKEN")

    def __init__(self):
        if not self.BOT_TOKEN:
            raise RuntimeError("BOT_TOKEN not set, create .env file with BOT_TOKEN")


conf = Config()
bot = Bot(conf.BOT_TOKEN)
dp = Dispatcher(bot)


keyboard_1 = InlineKeyboardMarkup()
keyboard_1.add(InlineKeyboardButton("[X] Флаг 1", callback_data="flag_1"))
keyboard_1.add(InlineKeyboardButton("[ ] Флаг 2", callback_data="flag_2"))

keyboard_2 = InlineKeyboardMarkup()
keyboard_2.add(InlineKeyboardButton("[ ] Флаг 1", callback_data="flag_1"))
keyboard_2.add(InlineKeyboardButton("[X] Флаг 2", callback_data="flag_2"))


@dp.message_handler(commands=["start"])
async def start(message: types.Message):
    await message.answer(f"Hello, {message.from_user.username}!\n"
                         f"Your chat id: {message.chat.id}\n"
                         f"Your user id: {message.from_user.id}",
                         reply_markup=keyboard_1)


@dp.callback_query_handler(lambda call: call.data == "flag_1")
async def flag_1_call(call: types.CallbackQuery):
    await call.message.edit_reply_markup(keyboard_1)


@dp.callback_query_handler(lambda call: call.data == "flag_2")
async def flag_2_call(call: types.CallbackQuery):
    await call.message.edit_reply_markup(keyboard_2)


if __name__ == '__main__':
    executor.start_polling(dp)
